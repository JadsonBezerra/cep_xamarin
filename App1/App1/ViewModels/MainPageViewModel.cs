﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App1.Models;
using Prism.Services;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class MainPageViewModel : BindableBase
    {
        #region Binding
        private string cep;
        public string CEP
        {
            get { return cep; }
            set { SetProperty(ref cep, value); }
        }
        private DelegateCommand enviarCEP;
        public DelegateCommand EnviarCEP
        {
            get { return enviarCEP; }
            set { SetProperty(ref enviarCEP, value); }
        }
        public string LogoCorreios { get; set; }
        private string resultadotring;
        public string ResultadoString
        {
            get { return resultadotring; }
            set { SetProperty(ref resultadotring, value); }
        }
        #endregion

        #region Global
        IPageDialogService _dialogService;
        #endregion

        public MainPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
        {
            LogoCorreios = "logoCorreios.png";
            _dialogService = dialogService;
            EnviarCEP = new DelegateCommand(BuscarCep);

        }

        private void BuscarCep()
        {
            try{
                if (IsCEPValid())
                {
                    Endereco Resultado = ViaCEPServico.BuscarEnderecoViaCEP(CEP.Trim());
                    if (Resultado == null)
                    {
                        _dialogService.DisplayAlertAsync("Erro", "Endereço não encontrado para CEP:" + cep, "OK");
                        return;
                    }
                    ResultadoString = string.Format("Endereço: {0}, {1}, {2},{3}", Resultado.localidade, Resultado.uf, Resultado.logradouro, Resultado.bairro);
                }
            }
            catch (Exception error)
            {
                _dialogService.DisplayAlertAsync("Erro", error.Message, "OK");
            }
        }

        private bool IsCEPValid()
        {
            bool valido = true;
            if (CEP.Trim().Length!=8)
            {
                _dialogService.DisplayAlertAsync("ERRO", "Cep deve conter exatamente 8 caracteres", "OK");
                valido= false;
            }
            if (!int.TryParse(CEP.Trim(),out _))
            {
                _dialogService.DisplayAlertAsync("ERRO", "Cep deve conter apenas números", "OK");
                valido= false;
            }
            return valido;
        }
    }
}
