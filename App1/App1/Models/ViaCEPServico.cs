﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using App1.Models;
using Newtonsoft.Json;

namespace App1.Models
{
    public static class ViaCEPServico
    {
        private static readonly string URL = "http://viacep.com.br/ws/{0}/json";

        public static Endereco BuscarEnderecoViaCEP(string cep)
        {
            string URLcompleto = string.Format(URL, cep);
            WebClient clienteWeb = new WebClient();
            string resposta = clienteWeb.DownloadString(URLcompleto);
            Endereco respostaFinal = JsonConvert.DeserializeObject<Endereco>(resposta);
            return respostaFinal.cep!=null?respostaFinal:null;
        }
    }
}
